import { combineReducers } from 'redux';
import landingManager from './landingManager';
import errorHandler from './errorHandler';
import todos from './todos';
import visibilityFilter from './visibilityFilter';

const todoReducer = combineReducers({
    todos,
    visibilityFilter
});

export default combineReducers({
    landingManager,
    errorHandler,
    todosApp: todoReducer
});

