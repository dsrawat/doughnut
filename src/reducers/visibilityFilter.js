import Constants from 'constants'

const initialState = Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_ALL;

const visibilityFilter = (state = initialState, action) => {

    const { type } = action;

    switch (type) {

        case Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_ALL:
            return Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_ALL;

        case Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_COMPLETED:
            return Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_COMPLETED

        case Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_PENDING:
            return Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_PENDING

        default:
            return state;
    }
}

export default visibilityFilter;
