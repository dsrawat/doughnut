import Constants from 'constants'


const initialState = []

const todos = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        case Constants.actions.TODOS_APP.TODOS.ADD_TODO:
            return [...state, { ...payload, completed: false }];

        case Constants.actions.TODOS_APP.TODOS.UPDATE_TODO:
            {
                let targetTodo = state.find(todo => todo.id === payload);
                targetTodo = { ...targetTodo };
                targetTodo.completed = true;

                let newState = [...state];
                newState = newState.filter(todo => todo.id !== payload);
                newState.push(targetTodo);

                return newState;
            }

        case Constants.actions.TODOS_APP.TODOS.DELETE_TODO:
            {
                let newState = [...state];
                newState = newState.filter(todo => todo.id !== payload);
                return newState;
            }

        default:
            return state;
    }
}

export default todos;