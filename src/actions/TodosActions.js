import Constants from 'constants'

export const addTodo = (todo) => ({
    type: Constants.actions.TODOS_APP.TODOS.ADD_TODO,
    payload: todo
})

export const updateTodo = (id) => ({
    type: Constants.actions.TODOS_APP.TODOS.UPDATE_TODO,
    payload: id
})

export const deleteTodo = (id) => ({
    type: Constants.actions.TODOS_APP.TODOS.DELETE_TODO,
    payload: id
})