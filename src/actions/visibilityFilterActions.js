import Constants from 'constants'

export const showAll = () => ({
    type: Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_ALL
})

export const showCompleted = () => ({
    type: Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_COMPLETED
})

export const showPending = (id) => ({
    type: Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_PENDING
})