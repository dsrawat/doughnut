import React from 'react'

const Todo = (props) => {

    const { todo, deleteTodo, updateTodo } = props;
    const { id, task, completed } = todo;

    return (
        <div className="todos__item grid grid-3--btn2 text-medium" >
            {
                completed ?
                    <i className="fa fa-check-circle-o todos__update" aria-hidden="true"></i> :
                    <i onClick={() => updateTodo(id)} className="fa fa-circle-o todos__update" aria-hidden="true"></i>
            }
            <div className="todos__task">{task}</div>
            <i onClick={() => deleteTodo(id)} className="fa fa-trash-o todos__delete" aria-hidden="true"></i>
        </div>
    )
}

export default Todo
