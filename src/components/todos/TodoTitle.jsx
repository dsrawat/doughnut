import React from 'react'

export default function TodoTitle() {
    return (
        <div className="todos__title">
            TODOs
        </div>
    )
}
