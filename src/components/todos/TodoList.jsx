import React from 'react'
import Constants from 'constants'
import Todo from './Todo'

const TodoList = (props) => {

    const { todos, updateTodo, deleteTodo, visibilityFilter } = props;

    return (
        <div className="todos__list">
            {
                todos.map(todo =>

                    visibilityFilter === Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_ALL ?
                        <Todo key={todo.id} todo={todo} updateTodo={updateTodo} deleteTodo={deleteTodo} /> :

                        visibilityFilter === Constants.actions.TODOS_APP.VISIBILITY_FILTER.SHOW_COMPLETED ?
                            todo.completed && <Todo key={todo.id} todo={todo} updateTodo={updateTodo} deleteTodo={deleteTodo} /> :

                            (!todo.completed) && <Todo key={todo.id} todo={todo} updateTodo={updateTodo} deleteTodo={deleteTodo} />
                )
            }
        </div>
    )
}

export default TodoList;
