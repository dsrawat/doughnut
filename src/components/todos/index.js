import React from 'react'
import TodoList from './TodoList'
import AddTodo from './AddTodo'
import TodoStatus from './TodoStatus'
import TodoFilters from './TodoFilters'
import TodoTitle from './TodoTitle';
import TodoProgressBar from './TodoProgressBar'

const Todos = (props) => {

    const { todos, addTodo, updateTodo, deleteTodo, visibilityFilter, showAll, showCompleted, showPending } = props;
    return (
        <div className="todos">
            <TodoTitle />
            <AddTodo addTodo={addTodo} />
            <TodoProgressBar todos={todos} />
            {/* <TodoStatus todos={todos} /> */}
            <hr />
            <TodoList todos={todos} updateTodo={updateTodo} deleteTodo={deleteTodo} visibilityFilter={visibilityFilter} />
            <TodoFilters showAll={showAll} showCompleted={showCompleted} showPending={showPending} visibilityFilter={visibilityFilter} />
        </div>
    )
}

export default Todos;