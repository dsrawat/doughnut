import React from 'react'

const TodoFilters = (props) => {

    const { showAll, showCompleted, showPending, visibilityFilter } = props;
    const customClass = visibilityFilter;

    return (
        <div className="todos__filter grid grid-3 height-medium">
            <button className={`text-medium ${customClass === 'SHOW_ALL' && customClass}`} onClick={showAll}>Show All</button>
            <button className={`text-medium ${customClass === 'SHOW_COMPLETED' && customClass}`} onClick={showCompleted}>Show Completed</button>
            <button className={`text-medium ${customClass === 'SHOW_PENDING' && customClass}`} onClick={showPending}>Show Pending</button>
        </div>
    )
}

export default TodoFilters
