import React from 'react'

const TodoProgressBar = (props) => {

    return (
        <div className="grid grid-2--stat ">
            <span >{calculateCompletedPercent()}%</span>
            <div className="todos__bar">
                <div className="todos__bar-fill" style={{ width: `${calculateCompletedPercent()}%` }}></div>
            </div>
        </div>
    )

    function calculateCompletedPercent() {
        if (props.todos.length === 0)
            return 0;

        const completed = props.todos.reduce((complete, todo) => todo.completed ? complete + 1 : complete + 0, 0);

        const total = props.todos.length;
        const completedPercent = (completed / total) * 100;

        return parseInt(completedPercent);
    }
}

export default TodoProgressBar
