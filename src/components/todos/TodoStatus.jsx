import React from 'react'

const TodoStatus = (props) => {
    return (
        <div className="todos__status grid grid-2 height-medium text-medium">
            <span className="text-center">TOTAL : {props.todos.length}</span>
            <span className="text-center"> COMPLETED : {calculateCompletedPercent()}%</span>
        </div>
    )

    function calculateCompletedPercent() {
        if (props.todos.length === 0)
            return 0;

        const completed = props.todos.reduce((complete, todo) => todo.completed ? complete + 1 : complete + 0, 0);

        const total = props.todos.length;
        const completedPercent = (completed / total) * 100;

        return completedPercent.toFixed(2);
    }
}

export default TodoStatus
