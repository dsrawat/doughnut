import React from 'react'

const GridPattern = (props) => {

    const { columns: col = 10, color = "primary", size = "medium", gap = "small" } = props;

    //colarr => array [0---columns-1]
    const colarr = Array.from(Array(col).keys());

    return (
        <div className={`grid grid-${col}  grid--size-${size} grid--gap-${gap} grid--margin-small`}>


            {
                colarr.map(format)
            }
        </div>
    )

    function format(val) {
        return (
            <div className={`grid__item-span1col-${val + 1} bgcolor-${color} border grid__item--pos-center`}>
                <span className="text-large">{val + 1}</span>
            </div>
        )
    }
}

export default GridPattern;
