import React from 'react'
import GridPattern from './GridPattern'

const Grid = () => {
    const gridPatterns = [
        { color: "primary", size: "medium", columns: 20 },
        { color: "secondary", size: "medium", columns: 15 },
        { color: "warning", size: "medium", columns: 10 },
        { color: "secondary", size: "medium", columns: 5 },
        { color: "warning", size: "medium", columns: 2 }
    ];

    return (
        <React.Fragment>
            {
                gridPatterns.map((gridProps, index) =>
                    <GridPattern key={index} color={gridProps.color} columns={gridProps.columns} size={gridProps.size} />
                )
            }
        </React.Fragment>
    )
}

export default Grid;
