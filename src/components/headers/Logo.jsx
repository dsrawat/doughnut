import React from 'react'

const Logo = () => {
    return (
        <div className="grid__item-span2col-1 grid__item--padding-small grid__item--style-logo">
            React Site
        </div>
    )
}

export default Logo
