import React from 'react'

const Name = () => {
    return (
        <ul className="grid__item-span3col-3 list list--style-none list--style-text grid grid-4">
            <li className="list__item-display-inline-block">Home</li>
            <li className="list__item-display-inline-block">About Us</li>
            <li className="list__item-display-inline-block">Login</li>
            <li className="list__item-display-inline-block">Logout</li>
        </ul>
    )
}

export default Name