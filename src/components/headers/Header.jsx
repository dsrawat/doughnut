import React from 'react'
import Logo from './Logo'
import Name from './Name'

const Header = () => {
    return (
        <header className="grid grid-5 text-medium">
            <Logo />
            <Name />
        </header>
    )
}

export default Header

