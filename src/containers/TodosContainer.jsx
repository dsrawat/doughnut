import React from 'react'
import { connect } from 'react-redux'
import Todos from '../components/todos';
import { addTodo, updateTodo, deleteTodo } from '../actions/TodosActions';
import { showAll, showCompleted, showPending } from '../actions/visibilityFilterActions'

const TodosContainer = props => <Todos {...props} />

const mapStateToProps = (state) => ({
    todos: state.todosApp.todos,
    visibilityFilter: state.todosApp.visibilityFilter
})

const mapDispatchToProps = dispatch => {
    return {
        addTodo: (todo) => dispatch(addTodo(todo)),
        updateTodo: (id) => dispatch(updateTodo(id)),
        deleteTodo: (id) => dispatch(deleteTodo(id)),
        showAll: () => dispatch(showAll()),
        showCompleted: () => dispatch(showCompleted()),
        showPending: () => dispatch(showPending())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosContainer)
