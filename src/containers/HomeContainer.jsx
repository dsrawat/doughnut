import React, { Component, Fragment } from 'react';
import APP from 'app';
import { connect } from 'react-redux';
import Header from '../components/headers/Header';
import Grid from '../components/grids/Grid';
import Gallery from '../components/gallerys/Gallery';
import TodosContainer from './TodosContainer';

class HomeContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="home__container">
                {/* <Header />
                <Gallery />
                <Grid /> */}
                <TodosContainer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { landingManager = {} } = state;
    return {
        landingManager
    }
};

export default connect(mapStateToProps, null)(HomeContainer);